<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('display_menu'))
{
    function display_menu($page)
    {
    	$CI =& get_instance();
        ?>
			<li <?php if($page == "home") { ?>class="current"<?php } ?>><a href="<?php echo base_url(); ?>" class="no-submenu"><span class="awe-home"></span>Home</a></li>
			<li <?php if($page == "mails") { ?>class="current"<?php } ?>>
				<a href="#"><span class="awe-file"></span>Messages</a>
				<ul>
					<li><a href="<?php echo base_url(); ?>mail/yourMails/">Inbox</a></li>
					<li><a href="<?php echo base_url(); ?>mail/sentMails/">Sent Messages</a></li>
					<li><a href="<?php echo base_url(); ?>mail/yourMails/">Deleted Messages</a></li>
				</ul>
			</li>	
			<li <?php if($page == "todo") { ?>class="current"<?php } ?>>
				<a href="#"><span class="awe-tasks"></span>TODO List</a>
				<ul>
					<li><a href="<?php echo base_url(); ?>todo/userTodoList/<?php echo $CI->session->userdata('userId'); ?>/">My TODO List</a></li>
					<li><a href="<?php echo base_url(); ?>todo/friendTodoList/">My Friends TODO List</a></li>
					<li><a href="<?php echo base_url(); ?>todo/userTodoList/<?php echo $CI->session->userdata('userId'); ?>/">All TODO List</a></li>
					<li><a href="<?php echo base_url(); ?>todo/userTodoList/<?php echo $CI->session->userdata('userId'); ?>/">Popular TODO List</a></li>
				</ul>
			</li>	
			<li <?php if($page == "users") { ?>class="current"<?php } ?>>
				<a href="#"><span class="awe-group"></span>Users</a>
				<ul>
					<li><a href="<?php echo base_url(); ?>users/myFriends/">My Friends</a></li>
					<li><a href="<?php echo base_url(); ?>users/">All Users</a></li>
				</ul>
			</li>        
        <?php
    }   

	function display_footer() {
		?>
		<!-- Main page footer -->
		<footer class="container">
			<p>All rights reserved &copy; <a href="<?php echo base_url(); ?>">Sasniedz.lv</a>.</p>
			<ul>
				<li><a href="#" class="">Support</a></li>
				<li><a href="#" class="">Terms & Conditions</a></li>
			</ul>
			<a href="#top" class="btn btn-primary btn-flat pull-right">Top &uarr;</a>
		</footer>
		<!-- /Main page footer -->		
		<?php
	}
	
	function display_motd() {
		?>
		<!-- Sample side note -->
		<section class="side-note">
			<div class="side-note-container">
				<h2>Message from Sasniedz.lv</h2>
				<p>Currently, this site is in development process, so please contact us if you find out any type of bugs!</p>
			</div>
			<div class="side-note-bottom"></div>
		</section>
		<!-- /Sample side note -->		
		<?php
	}
}