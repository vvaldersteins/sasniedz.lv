<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>Registration | Sasniedz.lv -- Social Portal</title>
		<meta name="description" content="">
		<meta name="author" content="Walking Pixels | www.walkingpixels.com">
		<meta name="robots" content="index, follow">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- CSS styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/huraga-red.css'>
		
		<!-- Fav and touch icons -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>img/icons/favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-57-precomposed.png">
		
		<!-- JS Libs -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>js/libs/jquery.js"><\/script>')</script>
		<script src="<?php echo base_url(); ?>js/libs/modernizr.js"></script>
		<script src="<?php echo base_url(); ?>js/libs/selectivizr.js"></script>
		
		<script>
			$(document).ready(function(){
				
				// Tooltips
				$('[title]').tooltip({
					placement: 'top'
				});
				
			});
		</script>
	</head>
	<body>
		
		<!-- Main page container -->
		<section class="container login" role="main">
			<?php if(isset($response)) { echo $response; } ?>
			<h1><a href="login.html" class="brand">Sasniedz</a></h1>
			<div class="data-block">
				<form method="post" action="<?php echo base_url(); ?>login/registrate/">
					<fieldset>
						<div class="control-group">
							<label class="control-label" for="login">Username</label>
							<div class="controls">
								<input type="text" placeholder="Your username" name="username">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="firstName">First Name</label>
							<div class="controls">
								<input type="text" placeholder="Your First Name" name="firstName">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="lastName">Last Name</label>
							<div class="controls">
								<input type="text" placeholder="Your Last Name" name="lastName">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="email">Email</label>
							<div class="controls">
								<input type="text" placeholder="Your Email" name="email">
							</div>
						</div>																		
						<div class="control-group">
							<label class="control-label" for="password">Password</label>
							<div class="controls">
								<input type="password" placeholder="Your Password" name="password">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="passwordAgain">Password Again</label>
							<div class="controls">
								<input type="password" placeholder="Your Password Again" name="passwordAgain">
							</div>
						</div>				
						<div class="control-group">
							<label class="control-label" for="about">About me</label>
							<div class="controls">
								<textarea style="height: 130px; width: 95.5%;" placeholder="Enter Information about yourself here..." name="about"></textarea>
							</div>
						</div>									
						<div class="form-actions">
							<button class="btn btn-large btn-inverse btn-alt" type="submit" name="registrate" value="ok"><span class="awe-signin"></span> Registrate</button>
						</div>
					</fieldset>
				</form>
			</div>
			<p><a href="<?php echo base_url(); ?>login/" class="pull-left"><small>Back to Login</small></a></p>
			
		</section>
		<!-- /Main page container -->
		
		<!-- Scripts -->
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-tooltip.js"></script>
		
	</body>
</html>
