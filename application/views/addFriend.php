<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>Add friend | Sasniedz.lv -- Social Portal</title>
		<meta name="description" content="">
		<meta name="author" content="Walking Pixels | www.walkingpixels.com">
		<meta name="robots" content="index, follow">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- jQuery TagsInput Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/plugins/jquery.tagsinput.css'>
		
		<!-- jQuery jWYSIWYG Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/plugins/jquery.jwysiwyg.css'>
		
		<!-- Bootstrap wysihtml5 Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/plugins/bootstrap-wysihtml5.css'>
		
		<!-- CSS styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/huraga-red.css'>
		
		<!-- Fav and touch icons -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>img/icons/favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-57-precomposed.png">
		
		<!-- JS Libs -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>js/libs/jquery.js"><\/script>')</script>
		<script src="<?php echo base_url(); ?>js/libs/modernizr.js"></script>
		<script src="<?php echo base_url(); ?>js/libs/selectivizr.js"></script>
		
		<script>
			$(document).ready(function(){
				
				// Tooltips
				$('[title]').tooltip({
					placement: 'top'
				});
				
				// Dropdowns
				$('.dropdown-toggle').dropdown();
				
				// Tabs
				$('.demoTabs a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');
				})
				
			});
		</script>
	</head>
	<body>
		
		<!-- Main page header -->
		<header class="container">
		
			<!-- Main page logo -->
			<h1><a href="login.html" class="brand">Sasniedz</a></h1>
						
			<!-- Alternative navigation -->
			<nav>
				<ul>
					<?php if($messages > 0) { ?><li><a href="<?php echo base_url(); ?>mail/yourMails/" style="color: #E74949;">You have <?php echo $messages; ?> new message<?php if($messages > 1) { echo 's'; } ?></a></li><?php } ?>					
					<?php if($requests > 0) { ?><li><a href="<?php echo base_url(); ?>users/yourRequests/" style="color: #E74949;">You have new friend requests</a></li><?php } ?>
					<li><a href="<?php echo base_url(); ?>homepage/logout/">Logout</a></li>
				</ul>
			</nav>
			<!-- /Alternative navigation -->
			
		</header>
		<!-- /Main page header -->
		
		<!-- Main page container -->
		<section class="container" role="main">
		
			<!-- Left (navigation) side -->
			<div class="navigation-block">
			
				<?php foreach($userData as $user) { ?>
					<!-- User profile -->
					<section class="user-profile">
						<figure class="clearfix">
							<img alt="<?php echo $user->username; ?> avatar" src="<?php echo $user->picture; ?>">
							<figcaption>
								<strong><a href="<?php echo base_url(); ?>users/view/<?php echo $user->id; ?>/" class=""><?php echo $user->firstName.' '.$user->surname; ?></a></strong>
								<em>AKA <?php echo $user->username; ?></em>
								<ul>
									<?php global $error; ?>
									<?php if($user->id != $this->session->userdata('userId')) { ?>
										<?php if(!empty($friends)) { ?>
											<?php foreach($friends as $friend) { ?>
												<?php if($friend->who == $user->id && $friend->with == $this->session->userdata('userId')) { $error = 1; } ?>
											<?php } ?>
										<?php } ?>
										<?php if($error != 1) { ?><li><a class="btn btn-primary btn-flat" href="<?php echo base_url(); ?>users/addFriend/<?php echo $user->id; ?>" title="Add to friends">Add friend</a></li><?php } ?>
										<li><a class="btn btn-primary btn-flat" href="<?php echo base_url(); ?>mail/send/<?php echo $user->id; ?>" title="Send message">Send IM</a></li>
									<?php } ?>
								</ul>
							</figcaption>
						</figure>
					</section>
					<!-- /User profile -->
				<?php } ?>
				
				<!-- Sample left search bar -->
				<form class="side-search">
					<input type="text" class="rounded" placeholder="To search type and hit enter">
				</form>
				<!-- /Sample left search bar -->
				
				<!-- Main navigation -->
				<nav class="main-navigation" role="navigation">
					<ul>
						<?php echo display_menu('users'); ?>																
					</ul>
				</nav>
				<!-- /Main navigation -->
				
				<?php echo display_motd(); ?>
				
			</div>
			<!-- Left (navigation) side -->
			
			<!-- Right (content) side -->
			<div class="content-block" role="main">
			
				<!-- Page header -->
				<article class="page-header">
					<h1>Add Friend</h1>
					<p>Please fill up all the fields and press Send invitation, to send your friend request to this user.</p>
				</article>
				<!-- /Page header -->
				
				<!-- Grid row -->
				<div class="row">
				
					<!-- Data block -->
					<article class="span12 data-block">
						<div class="data-container">
							<header>
								<h2>Add friend</h2>
							</header>
							<section class="tab-content">
							
								<!-- Tab #basic -->
								<div class="tab-pane active" id="basic">
								
									<!-- Example vertical forms -->
									<div class="row-fluid">
										<div class="span4">
											<p><?php if(isset($response)) { echo $response; } ?></p>
										</div>
										<div class="span8">
											<?php foreach($userData as $user) { ?>
												<form method="post" action="<?php echo base_url(); ?>users/sendFriendRequest/<?php echo $user->id; ?>/">
													<fieldset>													
														<div class="control-group">
															<label class="control-label" for="textarea">Description</label>
															<div class="controls">
																<textarea class="input-xlarge" name="description" rows="3"></textarea>
															</div>
														</div>
														<div class="form-actions">
															<button class="btn btn-alt btn-large btn-primary" type="submit" name="addFriend" value="ok">Send Invitation</button>
														</div>
													</fieldset>
												</form>
											<?php } ?>
										</div>
									</div>
									
								</div>
								<!-- /Tab #basic -->
								
							</section>
							<footer class="info">
								<p>Please keep in mind that you can't delete your friend request!</p>
							</footer>
						</div>
					</article>
					<!-- /Data block -->
					
				</div>
				<!-- /Grid row -->								
				
			</div>
			<!-- /Right (content) side -->
			
		</section>
		<!-- /Main page container -->
		
		<?php echo display_footer(); ?>
		
		<!-- Scripts -->
		<script src="<?php echo base_url(); ?>js/navigation.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-affix.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-tooltip.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-dropdown.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-tab.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-collapse.js"></script>
		
	</body>
</html>
