<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>Edit User | Sasniedz.lv -- Social Portal</title>
		<meta name="description" content="">
		<meta name="author" content="Walking Pixels | www.walkingpixels.com">
		<meta name="robots" content="index, follow">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- jQuery TagsInput Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/plugins/jquery.tagsinput.css'>
		
		<!-- jQuery jWYSIWYG Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/plugins/jquery.jwysiwyg.css'>
		
		<!-- Bootstrap wysihtml5 Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/plugins/bootstrap-wysihtml5.css'>
		
		<!-- CSS styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/huraga-red.css'>
		
		<!-- Fav and touch icons -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>img/icons/favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-57-precomposed.png">
		
		<!-- JS Libs -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>js/libs/jquery.js"><\/script>')</script>
		<script src="<?php echo base_url(); ?>js/libs/modernizr.js"></script>
		<script src="<?php echo base_url(); ?>js/libs/selectivizr.js"></script>
		
		<script>
			$(document).ready(function(){
				
				// Tooltips
				$('[title]').tooltip({
					placement: 'top'
				});
				
				// Dropdowns
				$('.dropdown-toggle').dropdown();
				
				// Tabs
				$('.demoTabs a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');
				})
				
			});
		</script>
	</head>
	<body>
		
		<!-- Main page header -->
		<header class="container">
		
			<!-- Main page logo -->
			<h1><a href="login.html" class="brand">Sasniedz</a></h1>
						
			<!-- Alternative navigation -->
			<nav>
				<ul>
					<?php if($messages > 0) { ?><li><a href="<?php echo base_url(); ?>mail/yourMails/" style="color: #E74949;">You have <?php echo $messages; ?> new message<?php if($messages > 1) { echo 's'; } ?></a></li><?php } ?>					
					<?php if($requests > 0) { ?><li><a href="<?php echo base_url(); ?>users/yourRequests/" style="color: #E74949;">You have new friend requests</a></li><?php } ?>
					<li><a href="<?php echo base_url(); ?>homepage/logout/">Logout</a></li>
				</ul>
			</nav>
			<!-- /Alternative navigation -->
			
		</header>
		<!-- /Main page header -->
		
		<!-- Main page container -->
		<section class="container" role="main">
		
			<!-- Left (navigation) side -->
			<div class="navigation-block">
			
				<?php foreach($userData as $user) { ?>
					<!-- User profile -->
					<section class="user-profile">
						<figure class="clearfix">
							<img alt="<?php echo $user->username; ?> avatar" src="<?php echo $user->picture; ?>">
							<figcaption>
								<strong><a href="<?php echo base_url(); ?>users/view/<?php echo $user->id; ?>/" class=""><?php echo $user->firstName.' '.$user->surname; ?></a></strong>
								<em>AKA <?php echo $user->username; ?></em>
								<ul>
									<li><a class="btn btn-primary btn-flat" href="<?php echo base_url(); ?>edit/password/" title="Change Password">Password</a></li>
									<li><a class="btn btn-primary btn-flat" href="<?php echo base_url(); ?>edit/user/" title="Account settings">Settings</a></li>
								</ul>
							</figcaption>
						</figure>
					</section>
					<!-- /User profile -->
				<?php } ?>
				
				<!-- Sample left search bar -->
				<form class="side-search">
					<input type="text" class="rounded" placeholder="To search type and hit enter">
				</form>
				<!-- /Sample left search bar -->
				
				<!-- Main navigation -->
				<nav class="main-navigation" role="navigation">
					<ul>
						<?php echo display_menu('home'); ?>																
					</ul>
				</nav>
				<!-- /Main navigation -->
				
				<?php echo display_motd(); ?>
				
			</div>
			<!-- Left (navigation) side -->
			
			<!-- Right (content) side -->
			<div class="content-block" role="main">
			
				<!-- Page header -->
				<article class="page-header">
					<h1>Edit Profile</h1>
					<p>Here you are able to edit your profile and add additional details.</p>
				</article>
				<!-- /Page header -->
				
				<!-- Grid row -->
				<div class="row">
				
					<!-- Data block -->
					<article class="span12 data-block">
						<div class="data-container">
							<header>
								<h2>Edit Profile</h2>
							</header>
							<section class="tab-content">
							
								<!-- Tab #basic -->
								<div class="tab-pane active" id="basic">
								
									<!-- Example vertical forms -->
									<div class="row-fluid">
										<div class="span4">
											<p>From here you are able to edit all of your account settings. To edit Password, go to Password edit page right next to your profile image.</p>
											<p><?php if(isset($response)) { echo $response; } ?></p>
										</div>
										<div class="span8">
											<?php foreach($userData as $user) { ?>
												<form method="post" action="<?php echo base_url(); ?>edit/editUser/" enctype="multipart/form-data">
													<fieldset>
														<div class="control-group">
															<label class="control-label" for="input">First Name</label>
															<div class="controls">
																<input class="input-xlarge" type="text" <?php if(isset($user->firstName)) { echo "value=\"$user->firstName\""; } ?> name="firstName">
																<p class="help-block">Your first name</p>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label" for="input">Last Name</label>
															<div class="controls">
																<input class="input-xlarge" type="text" <?php if(isset($user->surname)) { echo "value=\"$user->surname\""; } ?> name="lastName">
																<p class="help-block">Your last name</p>
															</div>
														</div>													
														<div class="control-group">
															<label class="control-label" for="select">Gender</label>
															<div class="controls">
																<select id="select" name="gender">
																	<option value="male" <?php if(isset($user->gender) && $user->gender == "male") { echo "selected=\"selected\""; } ?>>Male</option>
																	<option value="female" <?php if(isset($user->gender) && $user->gender == "female") { echo "selected=\"selected\""; } ?>>Female</option>
																</select>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label" for="input">Profile Picture</label>
															<div class="controls">
																<input class="input-xlarge" type="file" name="picture">
																<p class="help-block">Your profile picture</p>
															</div>
															<style>.thumbnail { margin-top: 10px;}</style>
															<?php if($user->gender == "male") { ?>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/user_m_suit-png/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/user_m_suit.png&w=150&h=150" alt="User male suit"  /></a>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/user_m_collar-png/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/user_m_collar.png&w=150&h=150" alt="User male collar" /></a>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/user_m_rib-png/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/user_m_rib.png&w=150&h=150" alt="User male rib"/></a>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/user_m_tie-png/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/user_m_tie.png&w=150&h=150" alt="User male tie" /></a>
															<?php } ?>
															<?php if($user->gender == "female") { ?>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/user_w_red-png/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/user_w_red.png&w=150&h=150" alt="User female red"/></a>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/user_w_green-png/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/user_w_green.png&w=150&h=150" alt="User female green" /></a>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/user_w_blue-png/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/user_w_blue.png&w=150&h=150" alt="User female blue"/></a>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/user_w_black-png/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/user_w_black.png&w=150&h=150" alt="User female black"/></a>
															<?php } ?>
															<?php if($user->gender == "") { ?>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/user_group_red-png/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/user_group_red.png&w=150&h=150" alt="User group red"/></a>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/user_group_green-png/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/user_group_green.png&w=150&h=150" alt="User group green"/></a>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/user_group_blue-png/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/user_group_blue.png&w=150&h=150" alt="User group blue"/></a>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/user_group_black-png/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/user_group_black.png&w=150&h=150" alt="User group black" /></a>
															<?php } ?>																														
															<?php if($user->uploadedPicture == "Y") { ?>
																<a class="thumbnail" href="<?php echo base_url(); ?>edit/changePicture/userPicture-<?php echo $user->id; ?>-jpg/"><img src="<?php echo base_url(); ?>/timthumb.php?src=<?php echo base_url(); ?>userPictures/userPicture-<?php echo $user->id; ?>.jpg&w=150&h=150" alt="Personal picture" /></a>
															<?php } ?>
														</div>														
														<div class="control-group">
															<label class="control-label" for="textarea">About</label>
															<div class="controls">
																<textarea class="input-xlarge" name="about" rows="3"><?php if(isset($user->about)) { echo $user->about; } ?></textarea>
															</div>
														</div>
														<div class="form-actions">
															<button class="btn btn-alt btn-large btn-primary" type="submit" name="editUser" value="ok">Save changes</button>
														</div>
													</fieldset>
												</form>
											<?php } ?>
										</div>
									</div>
									
								</div>
								<!-- /Tab #basic -->
								
							</section>
							<footer class="info">
								<p>Please keep in mind that you can't change your username and email address!</p>
							</footer>
						</div>
					</article>
					<!-- /Data block -->
					
				</div>
				<!-- /Grid row -->								
				
			</div>
			<!-- /Right (content) side -->
			
		</section>
		<!-- /Main page container -->
		
		<?php echo display_footer(); ?>
		
		<!-- Scripts -->
		<script src="<?php echo base_url(); ?>js/navigation.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-affix.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-tooltip.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-dropdown.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-tab.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-collapse.js"></script>
		
	</body>
</html>
