<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>Edit Password | Sasniedz.lv -- Social Portal</title>
		<meta name="description" content="">
		<meta name="author" content="Walking Pixels | www.walkingpixels.com">
		<meta name="robots" content="index, follow">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- jQuery TagsInput Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/plugins/jquery.tagsinput.css'>
		
		<!-- jQuery jWYSIWYG Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/plugins/jquery.jwysiwyg.css'>
		
		<!-- Bootstrap wysihtml5 Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/plugins/bootstrap-wysihtml5.css'>
		
		<!-- CSS styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/huraga-red.css'>
		
		<!-- Fav and touch icons -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>img/icons/favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-57-precomposed.png">
		
		<!-- JS Libs -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>js/libs/jquery.js"><\/script>')</script>
		<script src="<?php echo base_url(); ?>js/libs/modernizr.js"></script>
		<script src="<?php echo base_url(); ?>js/libs/selectivizr.js"></script>
		
		<script>
			$(document).ready(function(){
				
				// Tooltips
				$('[title]').tooltip({
					placement: 'top'
				});
				
				// Dropdowns
				$('.dropdown-toggle').dropdown();
				
				// Tabs
				$('.demoTabs a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');
				})
				
			});
		</script>
	</head>
	<body>
		
		<!-- Main page header -->
		<header class="container">
		
			<!-- Main page logo -->
			<h1><a href="login.html" class="brand">Sasniedz</a></h1>
						
			<!-- Alternative navigation -->
			<nav>
				<ul>
					<?php if($messages > 0) { ?><li><a href="<?php echo base_url(); ?>mail/yourMails/" style="color: #E74949;">You have <?php echo $messages; ?> new message<?php if($messages > 1) { echo 's'; } ?></a></li><?php } ?>					
					<?php if($requests > 0) { ?><li><a href="<?php echo base_url(); ?>users/yourRequests/" style="color: #E74949;">You have new friend requests</a></li><?php } ?>
					<li><a href="<?php echo base_url(); ?>homepage/logout/">Logout</a></li>
				</ul>
			</nav>
			<!-- /Alternative navigation -->
			
		</header>
		<!-- /Main page header -->
		
		<!-- Main page container -->
		<section class="container" role="main">
		
			<!-- Left (navigation) side -->
			<div class="navigation-block">
			
				<?php foreach($userData as $user) { ?>
					<!-- User profile -->
					<section class="user-profile">
						<figure class="clearfix">
							<img alt="<?php echo $user->username; ?> avatar" src="<?php echo $user->picture; ?>">
							<figcaption>
								<strong><a href="<?php echo base_url(); ?>users/view/<?php echo $user->id; ?>/" class=""><?php echo $user->firstName.' '.$user->surname; ?></a></strong>
								<em>AKA <?php echo $user->username; ?></em>
								<ul>
									<li><a class="btn btn-primary btn-flat" href="<?php echo base_url(); ?>edit/password/" title="Change Password">Password</a></li>
									<li><a class="btn btn-primary btn-flat" href="<?php echo base_url(); ?>edit/user/" title="Account settings">Settings</a></li>
								</ul>
							</figcaption>
						</figure>
					</section>
					<!-- /User profile -->
				<?php } ?>
				
				<!-- Sample left search bar -->
				<form class="side-search">
					<input type="text" class="rounded" placeholder="To search type and hit enter">
				</form>
				<!-- /Sample left search bar -->
				
				<!-- Main navigation -->
				<nav class="main-navigation" role="navigation">
					<ul>
						<?php echo display_menu('todo'); ?>																	
					</ul>
				</nav>
				<!-- /Main navigation -->
				
				<?php echo display_motd(); ?>
				
			</div>
			<!-- Left (navigation) side -->
			
			<!-- Right (content) side -->
			<div class="content-block" role="main">
			
				<!-- Page header -->
				<article class="page-header">
					<h1>Edit Todo</h1>
					<p>Here you are able to edit your selected todo details.</p>
				</article>
				<!-- /Page header -->
				
				<!-- Grid row -->
				<div class="row">
				
					<!-- Data block -->
					<article class="span12 data-block">
						<div class="data-container">
							<header>
								<h2>Edit Todo</h2>
							</header>
							<section class="tab-content">
							
								<!-- Tab #basic -->
								<div class="tab-pane active" id="basic">
								
									<!-- Example vertical forms -->
									<div class="row-fluid">
										<div class="span4">
											<p><?php if(isset($response)) { echo $response; } ?></p>
										</div>
										<div class="span8">
											<section>
												<form method="post" action="<?php echo base_url(); ?>todo/editTodo/<?php echo $this->uri->segment(3); ?>/">
													<?php foreach ($todos as $todo) { ?>
														<fieldset>
															<div class="control-group">
																<label class="control-label" for="subject">Title</label>
																<div class="controls">
																	<input id="subject" type="text" value="<?php echo $todo->title; ?>" name="title">
																</div>
															</div>
															<div class="control-group">
																<label class="control-label" for="message">Description</label>
																<div class="controls">
																	<textarea id="message" rows="5" name="description"><?php echo $todo->description; ?></textarea>
																</div>
															</div>
															<div class="form-actions">
																<button class="btn btn-primary" type="submit" name="editTodo" value="ok">Submit TODO Item</button>
															</div>
														</fieldset>
													<?php } ?>
												</form>
											</section>
											<footer class="info">
												<p>Please keep in mind that you can select either to show the TODO List Item for all people or only for your friends.</p>
											</footer>
										</div>
									</div>
									
								</div>
								<!-- /Tab #basic -->
								
							</section>
							<footer class="info">
								<p>Please keep in mind that you will need to know your current password to change your password!</p>
							</footer>
						</div>
					</article>
					<!-- /Data block -->
					
				</div>
				<!-- /Grid row -->								
				
			</div>
			<!-- /Right (content) side -->
			
		</section>
		<!-- /Main page container -->
		
		<?php echo display_footer(); ?>
		
		<!-- Scripts -->
		<script src="<?php echo base_url(); ?>js/navigation.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-affix.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-tooltip.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-dropdown.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-tab.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-collapse.js"></script>
		
	</body>
</html>
