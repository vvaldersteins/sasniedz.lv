<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>My Friends Todo's | Sasniedz.lv -- Social Portal</title>
		<meta name="description" content="">
		<meta name="author" content="Walking Pixels | www.walkingpixels.com">
		<meta name="robots" content="index, follow">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- jQuery Visualize Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/plugins/jquery.visualize.css'>
		
		<!-- jQuery jGrowl Styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/plugins/jquery.jgrowl.css'>
		
		<!-- CSS styles -->
		<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/huraga-red.css'>
		
		<!-- Fav and touch icons -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>img/icons/favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>img/icons/apple-touch-icon-57-precomposed.png">
		
		<!-- JS Libs -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>js/libs/jquery.js"><\/script>')</script>
		<script src="<?php echo base_url(); ?>js/libs/modernizr.js"></script>
		<script src="<?php echo base_url(); ?>js/libs/selectivizr.js"></script>
		
		<script>
			$(document).ready(function(){
				
				// Tooltips
				$('[title]').tooltip({
					placement: 'top'
				});
				
			});
		</script>
	</head>
	<body>
		
		<!-- Main page header -->
		<header class="container">
		
			<!-- Main page logo -->
			<h1><a href="login.html" class="brand">Sasniedz</a></h1>
						
			<!-- Alternative navigation -->
			<nav>
				<ul>
					<?php if($messages > 0) { ?><li><a href="<?php echo base_url(); ?>mail/yourMails/" style="color: #E74949;">You have <?php echo $messages; ?> new message<?php if($messages > 1) { echo 's'; } ?></a></li><?php } ?>					
					<?php if($requests > 0) { ?><li><a href="<?php echo base_url(); ?>users/yourRequests/" style="color: #E74949;">You have new friend requests</a></li><?php } ?>
					<li><a href="<?php echo base_url(); ?>homepage/logout/">Logout</a></li>
				</ul>
			</nav>
			<!-- /Alternative navigation -->
			
		</header>
		<!-- /Main page header -->
		
		<!-- Main page container -->
		<section class="container" role="main">
		
			<!-- Left (navigation) side -->
			<div class="navigation-block">
			
				<?php foreach($userData as $user) { ?>
					<!-- User profile -->
					<section class="user-profile">
						<figure class="clearfix">
							<img alt="<?php echo $user->username; ?> avatar" src="<?php echo $user->picture; ?>">
							<figcaption>
								<strong><a href="<?php echo base_url(); ?>users/view/<?php echo $user->id; ?>/" class=""><?php echo $user->firstName.' '.$user->surname; ?></a></strong>
								<em>AKA <?php echo $user->username; ?></em>
								<ul>
									<?php global $error; ?>
									<?php if($user->id != $this->session->userdata('userId')) { ?>
										<?php if(!empty($friends)) { ?>
											<?php foreach($friends as $friend) { ?>
												<?php if($friend->who == $user->id && $friend->with == $this->session->userdata('userId')) { $error = 1; } ?>
											<?php } ?>
										<?php } ?>
										<?php if($error != 1) { ?><li><a class="btn btn-primary btn-flat" href="<?php echo base_url(); ?>users/addFriend/<?php echo $user->id; ?>" title="Add to friends">Add friend</a></li><?php } ?>
										<li><a class="btn btn-primary btn-flat" href="<?php echo base_url(); ?>mail/send/<?php echo $user->id; ?>" title="Send message">Send IM</a></li>
									<?php } else { ?>
										<ul>
											<li><a class="btn btn-primary btn-flat" href="<?php echo base_url(); ?>edit/password/" title="Change Password">Password</a></li>
											<li><a class="btn btn-primary btn-flat" href="<?php echo base_url(); ?>edit/user/" title="Account settings">Settings</a></li>
										</ul>
									<?php } ?>
								</ul>
							</figcaption>
						</figure>
					</section>
					<!-- /User profile -->
				<?php } ?>
				
				<!-- Sample left search bar -->
				<form class="side-search">
					<input type="text" class="rounded" placeholder="To search type and hit enter">
				</form>
				<!-- /Sample left search bar -->
				
				<!-- Main navigation -->
				<nav class="main-navigation" role="navigation">
					<ul>
						<?php echo display_menu('todo'); ?>																
					</ul>
				</nav>
				<!-- /Main navigation -->
				
				<?php echo display_motd(); ?>
				
			</div>
			<!-- Left (navigation) side -->
			
			<!-- Right (content) side -->
			<div class="content-block" role="main">
			
				<?php foreach($userData as $user) { ?>
					<!-- Page header -->
					<article class="page-header">
						<h1>Welcome, <?php echo $user->username; ?>!</h1>
						<p><?php echo $user->about; ?></p>
					</article>
					<!-- /Page header -->
				<?php } ?>

				<!-- Grid row -->
				<div class="row">
					
					<!-- Data block -->
					<article class="span12 data-block todo-block">
						<div class="data-container">
							<header>
								<h2><span class="awe-edit"></span>Your Friends Latest Todo's!</h2>							
							</header>
							<section>
								<table class="table">
									<tbody>
										<?php if(!empty($todoItems)) { foreach($todoItems as $todoItem) { ?>
											<tr>
												<td>
													<p><?php if($todoItem->finished == "Y") { ?><span class="label label-success">Finished</span><?php } ?> <a href="<?php echo base_url(); ?>todo/view/<?php echo $todoItem->id; ?>/"><?php echo $todoItem->title; ?></a></p>
													<span>Created On: <time><?php echo $todoItem->creationDate; ?></time></span>
													<span><b><a href="<?php echo base_url(); ?>users/view/<?php echo $todoItem->userId; ?>/">By: <?php echo $todoItem->username; ?></a></b></span>
												</td>
											</tr>
										<?php } } ?>
									</tbody>
								</table>
							</section>
							<center><?php echo $pagination; ?></center>
						</div>
					</article>
					<!-- /Data block -->
					
				</div>
				<!-- /Grid row -->
				
			</div>
			<!-- /Right (content) side -->
			
		</section>
		<!-- /Main page container -->
		
		<?php echo display_footer(); ?>
		
		<!-- Scripts -->
		<script src="<?php echo base_url(); ?>js/navigation.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-affix.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-tooltip.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap/bootstrap-collapse.js"></script>
		
		<!-- Block TODO list -->
		<script>
			$(document).ready(function() {
				
				$('.todo-block input[type="checkbox"]').click(function(){
					$(this).closest('tr').toggleClass('done');
				});
				$('.todo-block input[type="checkbox"]:checked').closest('tr').addClass('done');
				
			});
		</script>
		
		<!-- jQuery Visualize -->
		<!--[if lte IE 8]>
			<script language="javascript" type="text/javascript" src="js/plugins/visualize/excanvas.js"></script>
		<![endif]-->
		<script src="<?php echo base_url(); ?>js/plugins/visualize/jquery.visualize.min.js"></script>
		
		<script>
			$(document).ready(function() {
			
				var chartWidth = $(('.chart')).parent().width()*0.9;
				
				$('.chart').hide().visualize({
					type: 'pie',
					width: chartWidth,
					height: chartWidth,
					colors: ['#389abe','#fa9300','#6b9b20','#d43f3f','#8960a7','#33363b','#b29559','#6bd5b1','#66c9ee'],
					lineDots: 'double',
					interaction: false
				});
			
			});
		</script>
		
		<!-- jQuery Flot Charts -->
		<!--[if lte IE 8]>
			<script language="javascript" type="text/javascript" src="js/plugins/flot/excanvas.min.js"></script>
		<![endif]-->
		<script src="<?php echo base_url(); ?>js/plugins/flot/jquery.flot.js"></script>
		
		<script>
			$(document).ready(function() {
			
				// Demo #1
				// we use an inline data source in the example, usually data would be fetched from a server
				var data = [], totalPoints = 300;
				function getRandomData() {
					if (data.length > 0)
						data = data.slice(1);
				
					// do a random walk
					while (data.length < totalPoints) {
						var prev = data.length > 0 ? data[data.length - 1] : 50;
						var y = prev + Math.random() * 10 - 5;
						if (y < 0)
							y = 0;
						if (y > 100)
							y = 100;
						data.push(y);
					}
				
					// zip the generated y values with the x values
					var res = [];
					for (var i = 0; i < data.length; ++i)
						res.push([i, data[i]])
					return res;
				}
				
				// setup control widget
				var updateInterval = 30;
				$("#updateInterval").val(updateInterval).change(function () {
					var v = $(this).val();
					if (v && !isNaN(+v)) {
						updateInterval = +v;
					if (updateInterval < 1)
						updateInterval = 1;
					if (updateInterval > 2000)
						updateInterval = 2000;
					$(this).val("" + updateInterval);
					}
				});
				
				// setup plot
				var options = {
					series: { shadowSize: 0, color: '#389abe' }, // drawing is faster without shadows
					yaxis: { min: 0, max: 100 },
					xaxis: { show: false },
					grid: { backgroundColor: '#ffffff' }
				};
				var plot = $.plot($("#demo-1"), [ getRandomData() ], options);
				
				function update() {
					plot.setData([ getRandomData() ]);
					// since the axes don't change, we don't need to call plot.setupGrid()
					plot.draw();
					setTimeout(update, updateInterval);
				}
				
				update();
			
			});
		</script>
		
	</body>
</html>
