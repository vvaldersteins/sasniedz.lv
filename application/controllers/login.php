<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	// Load Login view
	public function index()
	{
		if($this->session->userdata('logged_in') != "TRUE") {
			$this->load->view('login');
		}
		else {
			redirect('');
		}
	}
	
	// Validate and login user if details are correct
	public function doLogin() {
		if($this->session->userdata('logged_in') != "TRUE") {
			$this->load->model("login_model");
			$data['response'] = $this->login_model->loginUser();
			if($data['response'] == "Success!") {
				redirect();
			}
			else {
				$this->load->view('login', $data);
			}
		}
		else {
			redirect('');
		}				
	}
	
	// Registration form generator
	public function registration() {
		if($this->session->userdata('logged_in') != "TRUE") {
			$this->load->view('registration');
		}
		else {
			redirect('');
		}		
	}
	
	// Registrate user if registration button is pressed
	public function registrate() {
		if($this->session->userdata('logged_in') != "TRUE") {
			$this->load->model("registrate_model");
			$data['response'] = $this->registrate_model->registrateUser();
			$this->load->view('registration', $data);
		}
		else {
			redirect('');
		}				
	}

	// Activate user if activation code is inserted correctly
	public function activate() {
		if($this->session->userdata('logged_in') != "TRUE" && $this->uri->segment(3) != "") {
			$this->load->model("registrate_model");
			$data['response'] = $this->registrate_model->activateUser($this->uri->segment(3));
			redirect('');
		}
		else {
			redirect('');
		}				
	}	
	
}