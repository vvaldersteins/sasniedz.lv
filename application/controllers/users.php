<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('logged_in') == "TRUE") {
			redirect('users/allUsers/');
		} else {
			redirect('login/');
		}
	}

	public function allUsers()
	{
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			// Set up pagination	
			$this->load->library('pagination');
			
			$config['base_url'] = base_url().'users/allUsers';
			$config['total_rows'] = $this->user_model->getUserCount($this->session->userdata('userId'));
			$config['per_page'] = 20; 
			$config['uri_segment'] = 3;
			
			$this->pagination->initialize($config); 		
			// Pagination setup ends		
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;							
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));
			$data['Users'] = $this->user_model->getUsers($config['per_page'], $page);
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['pagination'] = $this->pagination->create_links();
			$this->load->view('users', $data);
		} else {
			redirect('login/');
		}
	}
	
	// View Single User profile
	public function view() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model("todo_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$data['todoItems'] = $this->todo_model->getUserTodo($this->uri->segment(3), 5);						
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->uri->segment(3));	
			$data['friends'] = $this->user_model->getUserFriends($this->uri->segment(3), "all");		
			$this->load->view('singleView', $data);
		} else {
			redirect('login/');
		}
	}
	
	// Add friend function, which will open add friend form
	public function addFriend() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));					
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$data['friends'] = $this->user_model->getUserFriends($this->uri->segment(3), "all");	
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->uri->segment(3));			
			$this->load->view('addFriend', $data);
		} else {
			redirect('login/');
		}
	}

	// Validate and send friend request if everything is ok 
	public function sendFriendRequest() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));					
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->uri->segment(3));	
			$data['response'] = $this->user_model->sendFriendRequest($this->uri->segment(3));	
			$data['friends'] = $this->user_model->getUserFriends($this->uri->segment(3), "all");		
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$this->load->view('addFriend', $data);
		} else {
			redirect('login/');
		}		
	}
	
	// Show all friend requests for currently logged in user
	public function yourRequests() {
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("user_model");
			$this->load->model("checker_model");	
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));				
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));		
			$data['requestsCount'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));	
			$data['requests'] = $this->user_model->friendRequests($this->session->userdata('userId'));
			$this->load->view('friendRequests', $data);
		} else {
			redirect('login/');
		}		
	}	
	
	// Decline friend request
	public function friendDecline() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");					
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$this->user_model->declineFriend($this->uri->segment(3));
			redirect('users/yourRequests/');
		} else {
			redirect('login/');
		}		
	}
	
	// Accept friend request
	public function friendAccept() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3)) && is_numeric($this->uri->segment(4))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");		
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$this->user_model->acceptFriend($this->uri->segment(3), $this->uri->segment(4));
			redirect('users/yourRequests/');
		} else {
			redirect('login/');
		}		
	}	
	
	// Show All your friends
	public function myFriends() {
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));
			// Set up pagination	
			$this->load->library('pagination');
			
			$config['base_url'] = base_url().'users/myFriends/';
			$config['total_rows'] = $this->user_model->getUserFriendsCount($this->session->userdata('userId'));
			$config['per_page'] = 20; 
			$config['uri_segment'] = 3;
			
			$this->pagination->initialize($config); 		
			// Pagination setup ends		
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;							
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));
			$data['Users'] = $this->user_model->getUsersFriends($config['per_page'], $page);
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['pagination'] = $this->pagination->create_links();
			$this->load->view('users', $data);
		} else {
			redirect('login/');
		}		
	}
	
}