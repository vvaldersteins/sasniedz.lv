<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mail extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));
			$data['onlineUsers'] = $this->user_model->getUsers("online");
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['offlineUsers'] = $this->user_model->getUsers('offline');
			$this->load->view('users', $data);
		} else {
			redirect('login/');
		}
	}
	
	// Send message to friend function, which will open send message form
	public function send() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));					
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$data['friends'] = $this->user_model->getUserFriends($this->uri->segment(3), "all");	
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->uri->segment(3));			
			$this->load->view('sendMessage', $data);
		} else {
			redirect('login/');
		}
	}

	// Validate and send message to friend if everything is ok 
	public function sendMessage() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");	
			$this->load->model('mail_model');	
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->uri->segment(3));	
			$data['response'] = $this->mail_model->sendMail($this->uri->segment(3));		
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$this->load->view('sendMessage', $data);
		} else {
			redirect('login/');
		}		
	}

	// Show all user mails
	public function yourMails() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->session->userdata('userId'))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");	
			$this->load->model('mail_model');	
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));	
			$data['mails'] = $this->mail_model->getMails($this->session->userdata('userId'));		
			$data['requestsCount'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$this->load->view('yourMessages', $data);
		} else {
			redirect('login/');
		}			
	}
	
	// Show all user sent mails
	public function sentMails() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->session->userdata('userId'))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");	
			$this->load->model('mail_model');	
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));	
			$data['mails'] = $this->mail_model->getSentMails($this->session->userdata('userId'));		
			$data['requestsCount'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$this->load->view('yourMessages', $data);
		} else {
			redirect('login/');
		}			
	}	
	
	// Delete specific mail
	public function delete() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");	
			$this->load->model('mail_model');	
			$this->load->model('mail_model');
			$this->mail_model->deleteMail($this->uri->segment(3));
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));	
			$data['mails'] = $this->mail_model->getMails($this->session->userdata('userId'));		
			$data['requestsCount'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$this->load->view('yourMessages', $data);
		} else {
			redirect('login/');
		}		
	}

	// Mark specific mail as read
	public function read() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");	
			$this->load->model('mail_model');	
			$this->load->model('mail_model');
			$this->mail_model->readMail($this->uri->segment(3));
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));	
			$data['mails'] = $this->mail_model->getMails($this->session->userdata('userId'));		
			$data['requestsCount'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$this->load->view('yourMessages', $data);
		} else {
			redirect('login/');
		}		
	}	
	
	// Reply to specific mail
	public function reply() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3)) && is_numeric($this->uri->segment(4))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");	
			$this->load->model('mail_model');	
			$this->load->model('mail_model');
			$this->mail_model->readMail($this->uri->segment(4));
			redirect('mail/sendMessage/'.$this->uri->segment(3));
		} else {
			redirect('login/');
		}		
	}		
	
}