<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model("todo_model");
			$this->load->model('mail_model');
			$data['todoItems'] = $this->todo_model->getUserTodo($this->session->userdata('userId'), 5);
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));
			$this->load->view('home', $data);
		} else {
			redirect('login/');
		}
	}
	
	// Check if user is logged in, if he is, then do a logout event.
	public function logout() {
		if($this->session->userdata('logged_in') == "TRUE") {
			$userData = array(
				"status"		=> 0,
				"lastActivity"	=> date('Y-m-d H:i:s') 				
			);
			$this->db->where("id", $this->session->userdata('userId'));
			$this->db->update("users", $userData);					
			$userData = array('username' => '', 'userId' => '', 'logged_in' => FALSE);
			$this->session->unset_userdata($userData);	
			redirect('login/');
		} else {
			redirect('login/');
		}		
	}
}