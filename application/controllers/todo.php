<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Todo extends CI_Controller {

	// Load newest Todo List
	public function index()
	{
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model("todo_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$data['todoItems'] = $this->todo_model->getUserTodo($this->session->userdata('userId'), 5);			
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));
			$this->load->view('home', $data);
		} else {
			redirect('login/');
		}
	}
	
	// Load Todo edit view
	public function edit()
	{
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model('mail_model');
			$this->load->model('todo_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));				
			$data['todos']	= $this->todo_model->getTodo($this->uri->segment(3));
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));
			$this->load->view('editTodo', $data);
		} else {
			redirect('login/');
		}
	}	
	
	// Edit todo item and apply data to database
	public function editTodo() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("todo_model");
			$this->load->model("checker_model");
			$this->load->model("user_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));							
			$this->checker_model->updateUserTime($this->session->userdata('userId'));		
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));	
			$data['response'] = $this->todo_model->editTodo();
			if($data['response'] == "Success!") {
				redirect('todo/view/'.$this->uri->segment(3)."/");
			}
			else {	
				$this->load->view('editTodo', $data);
			}
		} else {
			redirect('login/');
		}
	}	
	
	// Select specific user Todo Item
	public function userTodoList() {
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("todo_model");
			$this->load->model("checker_model");
			$this->load->model("user_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));	
			// Set up pagination	
			$this->load->library('pagination');
			
			$config['base_url'] = base_url().'todo/userTodoList/'.$this->uri->segment(3).'/';
			$config['total_rows'] = $this->todo_model->userTodoCount($this->uri->segment(3));
			$config['per_page'] = 20; 
			$config['uri_segment'] = 4;
			
			$this->pagination->initialize($config); 		
			// Pagination setup ends		
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			$data['todoItems'] = $this->todo_model->getUserTodoPaged($this->uri->segment(3), $config['per_page'], $page);						
			$this->checker_model->updateUserTime($this->session->userdata('userId'));		
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->uri->segment(3));	
			$data['friends'] = $this->user_model->getUserFriends($this->uri->segment(3), "all");	
			
			$data['pagination'] = $this->pagination->create_links();
			$this->load->view('userTodoList', $data);
		} else {
			redirect('login/');
		}
	}
	
	// Borrow a todo item from a user
	public function borrow() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("todo_model");
			$this->load->model("checker_model");
			$this->load->model("user_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$data['todoItems'] = $this->todo_model->getUserTodo($this->session->userdata('userId'), 5);						
			$this->checker_model->updateUserTime($this->session->userdata('userId'));		
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));	
			$this->todo_model->borrowTodo($this->uri->segment(3));
			redirect('');
		} else {
			redirect('login/');
		}
	}
	
	// Show all friend todo list
	public function friendTodoList() {
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("todo_model");
			$this->load->model("checker_model");
			$this->load->model("user_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));	
			// Set up pagination	
			$this->load->library('pagination');
			
			$config['base_url'] = base_url().'todo/friendTodoList/';
			$config['total_rows'] = $this->todo_model->getFriendsTodoCount($this->session->userdata('userId'));
			$config['per_page'] = 20; 
			$config['uri_segment'] = 3;
			
			$this->pagination->initialize($config); 		
			// Pagination setup ends		
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;															
			$this->checker_model->updateUserTime($this->session->userdata('userId'));		
			$data['todoItems'] = $this->todo_model->getFriendsTodoPaged($this->session->userdata('userId'), $config['per_page'], $page);
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));	
			$data['friends'] = $this->user_model->getUserFriends($this->session->userdata('userId'), "all");	
			$data['pagination'] = $this->pagination->create_links();
			$this->load->view('friendsTodoList', $data);
		} else {
			redirect('login/');
		}
	}			
	
	// Add new todo item to database
	public function add() {
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("todo_model");
			$this->load->model("checker_model");
			$this->load->model("user_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$data['todoItems'] = $this->todo_model->getUserTodo($this->session->userdata('userId'), 5);						
			$this->checker_model->updateUserTime($this->session->userdata('userId'));		
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));	
			$data['response'] = $this->todo_model->addTodo();
			if($data['response'] == "Success!") {
				redirect('');
			}
			else {	
				$this->load->view('home', $data);
			}
		} else {
			redirect('login/');
		}
	}
	
	// View single Todo item
	public function view() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model("todo_model");	
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));				
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$data['todoDatas'] = $this->todo_model->getTodo($this->uri->segment(3));
			if($data['todoDatas'][0]->borrowed_from != "") {
				$data['borrowedFrom'] = $this->user_model->getUser($data['todoDatas'][0]->borrowed_from);
			}
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));
			$this->load->view('singleTodo', $data);
		} else {
			redirect('login/');
		}		
	}
	
	// Delete Todo item
	public function delete() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("checker_model");
			$this->load->model("todo_model");		
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$this->todo_model->deleteTodo($this->uri->segment(3));			
			redirect('');
		} else {
			redirect('login/');
		}		
	}

	// Complete Todo item
	public function complete() {
		if($this->session->userdata('logged_in') == "TRUE" && is_numeric($this->uri->segment(3))) {
			$this->load->model("checker_model");
			$this->load->model("todo_model");		
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$this->todo_model->completeTodo($this->uri->segment(3));			
			redirect('');
		} else {
			redirect('login/');
		}		
	}
	
}