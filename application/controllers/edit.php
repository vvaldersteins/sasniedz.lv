<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit extends CI_Controller {

	// Generate default view
	public function index()
	{
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$this->checker_model->updateUserTime($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));
			$this->load->view('home', $data);
		} else {
			redirect('login/');
		}
	}
	
	// Generate Edit Password Form
	public function password() {
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));
			$this->load->view('editPassword', $data);
		} else {
			redirect('login/');
		}
	}	
	
	// Changes user picture
	public function changePicture() {
		if($this->session->userdata('logged_in') == "TRUE" && $this->uri->segment(3) != "") {
			$this->load->model("checker_model");
			$this->load->model('edit_model');
			$this->checker_model->updateUserTime($this->session->userdata('userId'));	
			$this->edit_model->changePicture($this->uri->segment(3));
			redirect('edit/user/');		
		} else {
			redirect('login/');
		}
	}
	
	// edit Password -- will validate and edit user password
	public function editPassword() {
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("edit_model");
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$data['response'] = $this->edit_model->editPassword($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));
			$this->load->view('editPassword', $data);
		} else {
			redirect('login/');
		}
	}	
	
	// Generate Edit User Form
	public function user() {
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));
			$this->load->view('editUser', $data);
		} else {
			redirect('login/');
		}
	}
	
	// edit User -- will validate and edit user data
	public function editUser() {
		if($this->session->userdata('logged_in') == "TRUE") {
			$this->load->model("edit_model");
			$this->load->model("user_model");
			$this->load->model("checker_model");
			$this->load->model('mail_model');
			$data['messages'] = $this->mail_model->messageCount($this->session->userdata('userId'));			
			$data['requests'] = $this->user_model->friendRequestCount($this->session->userdata('userId'));
			$this->checker_model->updateUserTime($this->session->userdata('userId'));			
			$data['response'] = $this->edit_model->editUser($this->session->userdata('userId'));
			$data['userData'] = $this->user_model->getUser($this->session->userdata('userId'));
			$this->load->view('editUser', $data);
		} else {
			redirect('login/');
		}
	}
	
}