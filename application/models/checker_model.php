<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checker_model extends CI_Model {
	
	// Update user last activity time
	function updateUserTime($id) {
		$userData = array(
			"lastActivity"	=> date('Y-m-d H:i:s') 				
		);
		$this->db->where("id", $id);
		$this->db->update("users", $userData);		
	}
	
}
