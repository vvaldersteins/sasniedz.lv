<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {
	
	// Check if user has pressed submit button and if all fields are filled
	public function loginUser() {
		if($this->input->post('login') == "ok") {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$response = "";
			// Validate user inserted data
			$query = $this->db->get_where('users', array("username" => $username, 'activated' => "Y"));
			foreach($query->result() as $row) {
				$userPassword = $this->encrypt->decode($row->password);
				$userId = $row->id;
			}
			if(isset($userPassword) && $userPassword == $password) {			
				$response .= "Success!";
				$sessionData = array(
				                   'username'  	=> $username,
				                   'userId'		=> $userId,
				                   'logged_in' 	=> TRUE
				);
				$this->session->set_userdata($sessionData);
				$userData = array(
					"status"				=> 1,
					"lastActivity"		 		=> date('Y-m-d H:i:s') 				
				);
				$this->db->where("id", $userId);
				$this->db->update("users", $userData);
			}
			else {
				$response .= "Error! Username or/and Password is not correct!";
			}
		}
		else {
			redirect('login/');
		}
		return $response;
	}
	
}
