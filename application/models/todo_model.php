<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Todo_model extends CI_Model {
	
	// Retreaves user todo count
	function userTodoCount($id) {
		if(is_numeric($id)) {
			return $this->db->get_where("todo", array('userId' => $id))->num_rows();
		}
		else {
			redirect('');
		}
	}
	
	// Add new Todo item to database
	function addTodo() {
		if($this->input->post('addTodo') == "ok") {
			$title = $this->input->post('title');
			$description = $this->input->post('description');
			$response = "";
			if(!isset($title) || $title == "" || strlen($title) <= 14 || strlen($title) > 150) {
				$response .= "Title is required, and it must be between 15 and 150 characters!<br />";	
			}
			if(!isset($description) || $description == "" || strlen($description) <= 14) {
				$response .= "Description is required, and it must be atleast 15 characters long!<br />";	
			}			
			if($response == "") {
				$data = array(
					"title"			=> htmlentities($title),
					"description"	=> htmlentities($description),
					"userId"		=> $this->session->userdata("userId"),
					"creationDate"		 	=> date('Y-m-d H:i:s'),
					'finished'		=> "N"
				);
				$this->db->insert('todo', $data);
				$response = "Success!";				
			}
			return $response;
		}
		else {
			redirect('login/');
		}
	}
	
	// Borrow a user todo item and insert it into db
	function borrowTodo($itemId) {
		$query = $this->db->get_where('todo', array('id' => $itemId));
		foreach($query->result() as $row) {
			$userId = $row->userId;
			$title = $row->title;
			$description = $row->description;
		}		
		$data = array(
			"title"			=> htmlentities($title),
			"description"	=> htmlentities($description),
			"userId"		=> $this->session->userdata("userId"),
			"creationDate"		 	=> date('Y-m-d H:i:s'),
			'finished'		=> "N",
			'borrowed_from' => $userId
		);
		$this->db->insert('todo', $data);
		$response = "Success!";				
	}	

	// Edit Todo item and apply data to database
	function editTodo() {
		if($this->input->post('editTodo') == "ok") {
			$title = $this->input->post('title');
			$description = $this->input->post('description');
			$response = "";
			if(!isset($title) || $title == "" || strlen($title) <= 14 || strlen($title) > 150) {
				$response .= "Title is required, and it must be between 15 and 150 characters!<br />";	
			}
			if(!isset($description) || $description == "" || strlen($description) <= 14) {
				$response .= "Description is required, and it must be atleast 15 characters long!<br />";	
			}			
			if($response == "") {
				$data = array(
					"title"			=> htmlentities($title),
					"description"	=> htmlentities($description),
					"userId"		=> $this->session->userdata("userId"),
					'finished'		=> "N"
				);
				$this->db->where('id', $this->uri->segment(3));
				$this->db->update('todo', $data);
				$response = "Success!";				
			}
			return $response;
		}
		else {
			redirect('login/');
		}
	}

	// Get specific user x todo items. If $count == 0 then returns all todo's!
	function getUserTodo($id, $count) {
		if(is_numeric($id)) {
			$this->db->order_by("id", "DESC");
			if($count == 0) {
				$query = $this->db->get_where('todo', array('userId' => $id));	
			} else {
				$query = $this->db->get_where('todo', array("userId" => $id), $count);
			}
			return $query->result();
		}
		else {
			redirect('login/');
		}
	}
	
	// Get specific user ($id), paged items. $count items per page. ($start), defines page.
	function getUserTodoPaged($id, $count, $start) {
		if(is_numeric($id)) {
			$this->db->order_by("id", "DESC");
			$this->db->limit($count, $start);
			$query = $this->db->get_where('todo', array("userId" => $id), $count);
			return $query->result();
		}
		else {
			redirect('login/');
		}
	}	
		
	// Get specific user ($id) all friends todo paged items. $count items per page. ($start), defines page.
	function getFriendsTodoPaged($id, $count, $start) {
		if(is_numeric($id)) {
			$ids = Array();
			$query = $this->db->get_where('friends', array('with' => $id));
			if($query->num_rows() > 0) {
				foreach($query->result() as $row) {
					array_push($ids, $row->who); 
				}
				$this->db->select('todo.id, todo.finished, users.username, todo.title, todo.creationDate, users.id as userId');
				$this->db->from('todo');			
				$this->db->order_by("todo.id", "DESC");
				$this->db->limit($count, $start);
				$this->db->where_in('todo.userId', $ids);
				$this->db->join('users', 'users.id = todo.userId');	
				$query = $this->db->get();	
				return $query->result();
			}
		}
		else {
			redirect('login/');
		}
	}
	
	// Get specific user all friends todo items count
	function getFriendsTodoCount($id) {
		if(is_numeric($id)) {
			$ids = Array();
			$query = $this->db->get_where('friends', array('with' => $id));
			if($query->num_rows() > 0) {
				foreach($query->result() as $row) {
					array_push($ids, $row->who); 
				}
				$this->db->select('todo.id, todo.finished, users.username, todo.title, todo.creationDate, users.id as userId');
				$this->db->from('todo');			
				$this->db->order_by("todo.id", "DESC");
				$this->db->where_in('todo.userId', $ids);
				$this->db->join('users', 'users.id = todo.userId');	
				$query = $this->db->get();	
				return $query->num_rows();
			}
		}
		else {
			redirect('login/');
		}
	}		
	
	// Get specific user all friends todo items
	function getFriendsTodo($id) {
		if(is_numeric($id)) {
			$ids = Array();
			$query = $this->db->get_where('friends', array('with' => $id));
			if($query->num_rows() > 0) {
				foreach($query->result() as $row) {
					array_push($ids, $row->who); 
				}
				$this->db->select('todo.id, todo.finished, users.username, todo.title, todo.creationDate, users.id as userId');
				$this->db->from('todo');			
				$this->db->order_by("todo.id", "DESC");
				$this->db->where_in('todo.userId', $ids);
				$this->db->join('users', 'users.id = todo.userId');	
				$query = $this->db->get();	
				return $query->result();
			}
		}
		else {
			redirect('login/');
		}
	}			
	
	// Get specific todo item
	function getTodo($id) {
		if(is_numeric($id)) {
			$this->db->select('*');
			$this->db->from('todo');
			$this->db->where('todo.id', $id); 
			$this->db->join('users', 'users.id = todo.userId');	
			$query = $this->db->get();			
			return $query->result();
		}
		else {
			redirect('login/');
		}
	}
	
	// Delete specific Todo item
	function deleteTodo($id) {
		if(is_numeric($id)) {
			$query = $this->db->get_where("todo", array("id" => $id));
			foreach($query->result() as $row) {
				if($row->userId == $this->session->userdata('userId')) {
					$this->db->delete("todo", array("id" => $id));
				}
			}			
		}
		else {
			redirect('login/');
		}		
	}
	
	// Complete specific Todo item
	function completeTodo($id) {
		if(is_numeric($id)) {
			$query = $this->db->get_where("todo", array("id" => $id));
			foreach($query->result() as $row) {
				if($row->userId == $this->session->userdata('userId')) {
					$data = array(
						"finished"		=> "Y",
						"finishDate" 	=> date('Y-m-d H:i:s')
					);
					$this->db->where('id', $id);
					$this->db->update('todo', $data);
				}
			}			
		}
		else {
			redirect('login/');
		}		
	}	
	
}
