<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrate_model extends CI_Model {
	
	public function activateUser($activationCode) {
		if($activationCode != "") {
			$query = $this->db->get_where('users', array('activated' => "N", 'activationCode' => $activationCode));
			if($query->num_rows() > 0) {
				$data = array(
					"activated"			=> "Y"
				);
				$this->db->where('activationCode', $activationCode);
				$this->db->update('users', $data);
			}	
		}
	}
	
	// Check if user has pressed submit button and if all fields are filled
	public function registrateUser() {
		if($this->input->post('registrate') == "ok") {
			$username = $this->input->post('username');
			$firstName = $this->input->post('firstName');
			$lastName = $this->input->post('lastName');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$passwordAgain = $this->input->post('passwordAgain');
			$about = $this->input->post('about');
			$response = "";
			// Validate user inserted data
			if(!isset($username) || $username == "" || strlen($username) <= 3 || strlen($username) > 15) {
				$response .= "Username is required, and it must be between 4 and 15 characters long<br />";
			}
			if(!isset($firstName) || $firstName == "" || strlen($firstName) <= 2 || strlen($firstName) > 15) {
				$response .= "First name is required, and it must be between 3 and 15 characters long<br />";
			}
			if(!isset($lastName) || $lastName == "" || strlen($lastName) <= 2 || strlen($lastName) > 20) {
				$response .= "Last name is required, and it must be between 3 and 20 characters long<br />";
			}
			if(!isset($email) || $email == "" || !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $email)) {
				$response .= "Email address is required, and it must be a valid email address<br />";
			}
			if(!isset($password) || $password == "" || strlen($password) < 8 || strlen($password) > 20) {
				$response .= "Password is required, and it must be between 8 and 20 characters long <br />";
			}
			if(!isset($passwordAgain) && $passwordAgain != $password) {
				$response .= "Passwords doesn't match! <br />";
			}
			if(!isset($about) || $about == "") {
				$response .= "About information is required!<br />";
			}
			$query = $this->db->get_where("users", array("username" => $username));
			if($query->num_rows() > 0) {
				$response .= "User with that username already exists!<br />";
			}
			$query = $this->db->get_where("users", array("email" => $email));
			if($query->num_rows() > 0) {
				$response .= "User with that email aldready exists!<br />";
			}	
			// If everything is okay, insert user in database
			if($response == "") {
				$password = $this->encrypt->encode($password);
				$activationCode = md5($password.$username.$firstName);
				$data = array(
					"username" 			=> $username,
					"email"				=> $email,
					"firstName"			=> $firstName,
					"surname"			=> $lastName,
					"password"			=> $password,
					"about"				=> $about,
					"picture"			=> base_url().'userPictures/user_group_red.png',
					"activated"			=> "N",
					"activationCode"	=> $activationCode,
					"level"				=> 0,
					"uploadedPicture"	=> "N",
					"date"		 		=> date('Y-m-d H:i:s') 
				);
				$this->db->insert('users', $data);
				// Sends user approve email
				$this->load->library('email');
				
				$this->email->from('donotreply@sasniedz.lv', 'Sasniedz.lv Administrators');
				$this->email->to($email); 
				
				$this->email->subject('Congratulations! You have been registered!');
				$this->email->message("Congratulations! \n You have been registered to sasniedz.lv portal. Please click this link - ".base_url()."login/activate/".$activationCode."/ to activate your account \n Thank you! \n Best regards, \n Sasniedz.lv Administrators");	
				
				$this->email->send();				
				$response = "Success! You have finished the registration process, please check your email!";
			}
		}
		else {
			redirect('login/registration/');
		}
		return $response;
	}
	
}
