<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mail_model extends CI_Model {
	
	// Will validate and send mail if everything is ok
	public function sendMail($id) {
		if($this->input->post('sendMessage') == "ok") {
			$description = $this->input->post('message');
			$title = $this->input->post('title');
			$response = "";
			if(!isset($description) || $description == "") {
				$response .= "Message is required!<br />";
			}				
			if(!isset($title) || $title == "" || strlen($title) > 99) {
				$response .= "Title is required!<br />";
			}
			if($response == "") {				
				$data = array(
					"senderId" 		=> $this->session->userdata('userId'),
					'receiverId' 	=> $id,
					'message'		=> htmlentities($description),
					'title'			=> htmlentities($title),
					'status'		=> 0,
					'date' 			=> date('Y-m-d H:i:s')
				);
				$this->db->insert('messages', $data);
				$response .= "Success!";
			}
			return $response;
		}
	}
	
	// Retrieves user mail count
	public function messageCount($id) {
		if(is_numeric($id)) {
			$query = $this->db->get_where('messages', array('receiverId' => $id, 'status' => 0));
			return $query->num_rows();
		} else {
			redirect('');
		}
	}
	
	// Retrieves messages for user
	public function getMails($id) {
		if(is_numeric($id)) {
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where(array('messages.receiverId' => $id)); 
			$this->db->order_by('messages.id', "DESC");
			$this->db->join('messages', 'users.id = messages.senderId');				
			$query = $this->db->get();
			return $query->result();
		} else {
			redirect('');
		}
	}	
	
	// Retrieves messages sent by user
	public function getSentMails($id) {
		if(is_numeric($id)) {
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where(array('messages.senderId' => $id)); 
			$this->db->order_by('messages.id', "DESC");
			$this->db->join('messages', 'users.id = messages.receiverId');				
			$query = $this->db->get();
			return $query->result();
		} else {
			redirect('');
		}
	}		
	
	// Deletes specific mail
	public function deleteMail($id) {
		if(is_numeric($id)) {
			$this->db->delete("messages", array("id" => $id, "receiverId" => $this->session->userdata('userId')));
		} else {
			redirect('');
		}
	}	
	
	// Mark specific mail as read
	public function readMail($id) {
		if(is_numeric($id)) {
			$data = array(
				"status"		=> "1"
			);
			$this->db->where(array('id' => $id, 'receiverId' => $this->session->userdata('userId')));
			$this->db->update('messages', $data);
		} else {
			redirect('');
		}
	}	
	
}
