<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit_model extends CI_Model {
	
	// Will validate and edit user password
	public function editPassword($id) {
		if(is_numeric($id) && $this->input->post('editPassword') == "ok") {
			$oldPassword = $this->input->post('oldPassword');
			$password = $this->input->post("newPassword");
			$passwordAgain = $this->input->post("newPasswordAgain");
			$response = "";
			// Validate User data			
			$query = $this->db->get_where('users', array("id" => $id));
			foreach($query->result() as $row) {
				$userPassword = $this->encrypt->decode($row->password);
			}			
			if($oldPassword != $userPassword) {
				$response .= "Old password is incorrect!";
			}
			if($response == "") {
				if(!isset($password) || $password == "" || strlen($password) < 8 || strlen($password) > 20) {
					$response .= "Password is required, and it must be between 8 and 20 characters long <br />";
				}
				if(!isset($passwordAgain) && $passwordAgain != $password) {
					$response .= "Passwords doesn't match! <br />";
				}
				if($response == "") {
					$password = $this->encrypt->encode($password);
					$userData = array(
						"password"	=> $password
					);
					$this->db->where('id', $id);
					$this->db->update('users', $userData);
					$response .= "Success! Your password has been changed!";
				}
			}
			return $response;
		}
		else {
			redirect('/login/');
		}
	}

	// Changes user picture
	public function changePicture($name) {
		$name = str_replace("-png", ".png", $name);
		$name = str_replace("-jpg", ".jpg", $name);
		$id = $name;
		$id = str_replace("userPicture-", "", $id);
		$id = str_replace(".jpg", "", $id);
		if(is_numeric($id) && $id != $this->session->userdata('userId')) {
			die('this is not your picture!');
		}
		if(isset($name) && $name != ""){
			$file = base_url()."userPictures/".$name;
			$file_headers = @get_headers($file);
			if($file_headers[0] == 'HTTP/1.0 404 Not Found'){
			  	return false;
			} else if ($file_headers[0] == 'HTTP/1.0 302 Found'){
			   	return false;
			} else {
				$userData = array(
					"picture"	=> $file
				);
				$this->db->where('id', $this->session->userdata('userId'));
				$this->db->update('users', $userData);
			}
		}
	}
	
	// Will validate and edit user
	public function editUser($id) {
		if(is_numeric($id) && $this->input->post('editUser') == "ok") {
			$firstName = $this->input->post('firstName');
			$lastName = $this->input->post('lastName');
			$gender = $this->input->post('gender');
			$picture = $this->input->post('picture');
			$about = $this->input->post('about');
			$uploadedPicture = "N";
			$response = "";
			// Validate user inserted data
			if(!isset($firstName) || $firstName == "" || strlen($firstName) <= 2 || strlen($firstName) > 15) {
				$response .= "First name is required, and it must be between 3 and 15 characters long<br />";
			}
			if(!isset($lastName) || $lastName == "" || strlen($lastName) <= 2 || strlen($lastName) > 20) {
				$response .= "Last name is required, and it must be between 3 and 20 characters long<br />";
			}
			if(!isset($about) || $about == "") {
				$response .= "About information is required!<br />";
			}
			if($response == "" && $_FILES['picture']['name'] != "") {
				// Validate Picture upload field
				$allowedExts = array("jpg");
				$extension = end(explode(".", $_FILES["picture"]["name"]));
				if ((($_FILES["picture"]["type"] == "image/gif") || ($_FILES["picture"]["type"] == "image/jpeg") || ($_FILES["picture"]["type"] == "image/pjpeg")) && ($_FILES["picture"]["size"] < 1000141) && in_array($extension, $allowedExts)) {
					if ($_FILES["picture"]["error"] > 0) {
				    	$response .= "Return Code: " . $_FILES["picture"]["error"] . "<br />";
				    }
				  	else
				    {
				    	$pictureName = "userPicture-".$id.".".$extension;
				      	move_uploaded_file($_FILES["picture"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . '/sasniedz/userPictures/' . $pictureName);
				    }
				}
				else
				{
				  $response .= "Allowed picture extension is JPG and picture must be below 1MB in size<br />".$_FILES["picture"]["type"];
				}
				$uploadedPicture = "Y";
			}
			if($response == "" && $_FILES['picture']['name'] == "") {
				$query = $this->db->get_where('users', array('id' => $id));
				foreach($query->result() as $row) {
					$pictureName = $row->picture;
				}
				$pictureName = str_replace(base_url()."userPictures/", "", $pictureName);
			}
			// Edit user details and save them in database
			if($response == "") {
				$userData = array(
					"firstName"	=> htmlentities($firstName),
					"surname"	=> htmlentities($lastName),
					"gender"	=> $gender,
					"about"		=> htmlentities($about),
					"picture"	=> base_url().'userPictures/'.$pictureName,
					'uploadedPicture' => $uploadedPicture
				);
				$this->db->where('id', $id);
				$this->db->update('users', $userData);
				$response = "Success! Your profile data was updated!";
			}
			return $response;			
		}
		else {
			redirect('/login/');
		}
	}
	
}
