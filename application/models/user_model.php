<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
	
	// Get User Details by User ID
	public function getUser($id) {
		if(is_numeric($id)) {
			$query = $this->db->get_where('users', array("id" => $id));
			return $query->result();
		}
	}
	
	// Get all users paged. $count - how much per page, $start which page.
	public function getUsers($count, $start) {
		$this->db->limit($count, $start);
		$query = $this->db->get("users");
		return $query->result();
	}
	
	// Get all user Count.
	public function getUserCount() {
		$query = $this->db->get("users");
		return $query->num_rows();		
	}
	
	// Get specific user friends -- "all" : Will retrieve all users ; "offline" : Will retrieve offline users ; "Online" : Will retrieve online users
	public function getUserFriends($id, $status) {
		if($status == "all") {
			$this->db->select('*');
			$this->db->from('friends'); 
			$this->db->where('friends.who', $id);
			$this->db->join('users', 'users.id = friends.with');						
			$query = $this->db->get();
			return $query->result();
		}
		else if($status == "online") {
			$this->db->select('*');
			$this->db->from('friends'); 
			$this->db->where(array('friends.who' => $id, 'users.status' => 1));
			$this->db->join('users', 'users.id = friends.with');				
			$query = $this->db->get();
			return $query->result();
		}
		else if($status == "offline") {
			$this->db->select('*');
			$this->db->from('friends'); 
			$this->db->where(array('friends.who' => $id, 'users.status' => 0));
			$this->db->join('users', 'users.id = friends.with');				
			$query = $this->db->get();
			return $query->result();
		}
	}	
	
	// Get all users wich are your friends paged. $count - how much per page, $start which page.
	public function getUsersFriends($count, $start) {
		$this->db->select('*');
		$this->db->limit($count, $start);
		$this->db->from('friends'); 
		$this->db->where('friends.who', $this->session->userdata('userId'));
		$this->db->join('users', 'users.id = friends.with');						
		$query = $this->db->get();
		return $query->result();
	}	
	
	public function getUserFriendsCount($id) {
		$this->db->select('*');
		$this->db->from('friends'); 
		$this->db->where('friends.who', $this->session->userdata('userId'));
		$this->db->join('users', 'users.id = friends.with');						
		$query = $this->db->get();
		return $query->num_rows();		
	}
	
	// Will validate and send friend request if everything is ok
	public function sendFriendRequest($id) {
		if($this->input->post('addFriend') == "ok") {
			$description = $this->input->post('description');
			$response = "";
			if(!isset($description) || $description == "") {
				$response .= "Invitation description is required!<br />";
			}				
			if($response == "") {
				if($id == $this->session->userdata('userId')) {
					$response .= "Sorry, but you can't send yourself a friend request!";
				}
				$query = $this->db->get_where('users', array('id' => $id));
				if($query->num_rows() < 1) {
					$response .= "No user with that id!";
				}
				$query = $this->db->get_where('friendRequests', array("receiverId" => $id, "senderId" => $this->session->userdata('userId')));
				if($query->num_rows() > 0) {
					$response .= "You have already sent invitation to this user!";	
				}
				$query = $this->db->get_where('friendRequests', array("receiverId" => $this->session->userdata('userId'), "senderId" => $id));
				if($query->num_rows() > 0) {
					$response .= "Sorry, this user has already sent you friend request! Please check your invitations!";	
				}				
				if($response == "") {
					$data = array(
						"senderId" 		=> $this->session->userdata('userId'),
						'receiverId' 	=> $id,
						'message'		=> htmlentities($description),
						'status'		=> 0,
						'date' 			=> date('Y-m-d H:i:s')
					);
					$this->db->insert('friendRequests', $data);
					$response .= "Success!";
				}
			}
			return $response;
		}
	}

	// Retrieves users friend request count
	public function friendRequestCount($id) {
		if(is_numeric($id)) {
			$query = $this->db->get_where('friendRequests', array('receiverId' => $id, 'status' => 0));
			return $query->num_rows();
		} else {
			redirect('');
		}
	}
	
	// Retrieves friend requests
	public function friendRequests($id) {
		if(is_numeric($id)) {
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where(array('friendRequests.receiverId' => $id, 'friendRequests.status' => 0)); 
			$this->db->join('friendRequests', 'users.id = friendRequests.senderId');				
			$query = $this->db->get();
			return $query->result();
		} else {
			redirect('');
		}
	}
	
	// Decline friend rqeuest
	public function declineFriend($id) {
		if(is_numeric($id)) {
			$this->db->delete("friendRequests", array("id" => $id, "receiverId" => $this->session->userdata('userId')));
		} else {
			redirect('');
		}
	}	
	
	// Accept friend rqeuest
	public function acceptFriend($id, $senderId) {
		if(is_numeric($id)) {
				$data = array(
					"status"		=> "1"
				);
				$this->db->where(array('id' => $id, 'receiverId' => $this->session->userdata('userId')));
				// Insert receiver friend in database
				$this->db->update('friendRequests', $data);
				$data = array(
					'who'			=> $this->session->userdata('userId'),
					'with'			=> $senderId
				);
				$this->db->insert('friends', $data);
				// Insert sender friend in database
				$data = array(
					'with'			=> $this->session->userdata('userId'),
					'who'			=> $senderId
				);
				$this->db->insert('friends', $data);				
		} else {
			redirect('');
		}
	}		
	
}
